using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Scenas : MonoBehaviour
{
    
    public void Niveles(string scene)
    {
        SceneManager.LoadScene(scene);
    }

}
